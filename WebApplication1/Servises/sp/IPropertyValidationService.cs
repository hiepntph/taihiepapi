﻿namespace WebApplication1.Servises.sp
{
    public interface IPropertyValidationService
    {
        bool HasValidProperties<T>(string fields);
    }
}
