﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Servises.sp
{
    public class PropertyMapping<TSource, TDestination> : IPropertyMappingMarker
    {
        public Dictionary<string, PropertyMapingValue> MappingDictionary { get; set; }
        public PropertyMapping(Dictionary<string, PropertyMapingValue> mappingDictionary)
        {
            MappingDictionary = mappingDictionary ?? throw new ArgumentNullException(nameof(mappingDictionary));
        }
    }
}
