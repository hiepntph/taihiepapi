﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApplication1.Servises.sp
{
    public interface IPropertyMappingService
    {
        Dictionary<string, PropertyMapingValue> GetPropertyMapping<TSource, TDestination>();
        bool ValidMappingExit<TSource, TDestination>(string fields);
    }
}
