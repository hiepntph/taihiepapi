﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inmergers.Common.Utils;
using WebApplication1.Entities;
using WebApplication1.Helpers;

namespace WebApplication1.Servises.bandandAlbum
{
    public interface IbandAndAlbumRepositorycs
    {
        #region albums
        //tập hợp các phần tử và trả về 1 phần tử
        IEnumerable<Album> getAlbums(Guid banId);
        Album getAlbum(Guid bandId, Guid albumId);
        void addAlbum(Guid bandId, Album album);
        void uppdateAlbum(Album album);
        void deleteAlbums(Album album);


        #endregion

        #region bandconvert
        Task<ResponseData.Response> createBand(BandCreateModel model);
        Task<ResponseData.Response>updateBand(Guid Id, BandUpdateModel model);
        Task<ResponseData.Response> getBand(BandQueryModel filter);
        Task<ResponseData.Response> getIdBand(Guid bandId);
        Task<ResponseData.Response> deleteBand(Guid bandId);

        #endregion
        #region albumconvert
        Task<ResponseData.Response> createAlbum(AlbumCreateModel model);
        Task<ResponseData.Response> getAlbums(AlbumQueryModel filter);
        Task<ResponseData.Response> getIdAlbum(Guid albumId);
        Task<ResponseData.Response> deleteAlbum(Guid albumId);
        Task<ResponseData.Response> updatelbum(Guid albumId,AlbumUpdateModel model);
        #endregion
        #region bandcu
        IEnumerable<Band> GetBands();
        PagedList<Band> GetBands(BandsResourceParameters band);
        Band getBand(Guid bandId);
        IEnumerable<Band> getBands(IEnumerable<Guid> bandIds);
        Task<Band> AddBand2(Band band);
        void AddBand(Band band);
        void UpdateBand(Band band);
        void DeleteBand(Band band);
        bool BandExit(Guid bandId);
        bool AlbumExit(Guid albumId);
        bool Save();
        #endregion
    }
}
