﻿using System;
using Inmergers.Common.Utils;
using WebApplication1.Entities;

namespace WebApplication1.Servises.bandandAlbum
{
    public class AlbumModel
    {
        public Guid Id { get; set; }
        public string Tille { get; set; }
        public string Description { get; set; }
        public Band bandAlbum { get; set; }
        // public Guid BanID { get; set; }
    }
    public class AlbumCreateModel
    {
        public string Tille { get; set; }
        public string Description { get; set; }
        public Guid Band { get; set; }
    }
    public class AlbumUpdateModel
    {
        public string Tille { get; set; }
        public string Description { get; set; }
    }
    public class AlbumQueryModel : PaginationRequest
    {
        public Guid? AlbumId { get; set; }
    }
}
