﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Common.Utils;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Serilog;
using WebApplication1.Dbcontexts;
using WebApplication1.Entities;
using WebApplication1.Helpers;
using WebApplication1.Models;
using WebApplication1.Models.NewFolder;
using WebApplication1.Servises.sp;
using Code = System.Net.HttpStatusCode;

namespace WebApplication1.Servises.bandandAlbum
{
    public class BanAndAlbumRepository : IbandAndAlbumRepositorycs
    {
        private readonly BandandAlbumDbcontexts _context;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly IMapper _mapper;
        public BanAndAlbumRepository(BandandAlbumDbcontexts context
                , IPropertyMappingService propertMappingService, IMapper mapper
        )
        {
            // được đưa một ngoại lệ chuỗi được gán cho một name thuộc context
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _propertyMappingService = propertMappingService ?? throw new ArgumentNullException(nameof(propertMappingService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        #region albumcu

        public IEnumerable<Album> getAlbums(Guid banId)
        {
            if (banId == Guid.Empty)
                throw new ArgumentNullException(nameof(banId));
            return _context.Albums.Where(c => c.BanID == banId).OrderBy(c => c.Tille).ToList();
        }

        public Album getAlbum(Guid bandId, Guid albumId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            if (albumId == Guid.Empty)
                throw new ArgumentNullException(nameof(albumId));
            return _context.Albums.Where(c => c.BanID == bandId && c.Id == albumId).FirstOrDefault();
        }

        public void addAlbum(Guid bandId, Album album)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            if (album == null)
                throw new ArgumentNullException(nameof(album));
            album.BanID = bandId;
            _context.Albums.Add(album);
        }

        public void uppdateAlbum(Album album)
        {
            // throw new NotImplementedException();
        }

        public void deleteAlbums(Album album)
        {
            if (album == null)
                throw new ArgumentNullException(nameof(album));

            _context.Albums.Remove(album);
        }

        #endregion
        #region CRUD Bandcu
        public IEnumerable<Band> getBands(IEnumerable<Guid> bandIds)
        {
            if (bandIds == null)
                throw new ArgumentNullException(nameof(bandIds));
            return _context.Bands.Where(c => bandIds.Contains(c.Id)).OrderBy(c => c.Id).ToList();
        }

        public IEnumerable<Band> GetBands()
        {
            return _context.Bands.ToList();
        }

        public Band getBand(Guid bandId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            return _context.Bands.FirstOrDefault(c => c.Id == bandId);
        }

        public PagedList<Band> GetBands(BandsResourceParameters band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));
            // if (string.IsNullOrWhiteSpace(band.loc) && string.IsNullOrWhiteSpace(band.tim))
            //      return GetBands();
            // tạo 1 biến kiểm tra và truy vấn dữ liệu 
            var collection = _context.Bands as IQueryable<Band>;

            if (!string.IsNullOrWhiteSpace(band.loc))
            {
                var mainGenre = band.loc.Trim();
                collection = collection.Where(c => c.MainGenre == mainGenre);
            }
            if (!string.IsNullOrWhiteSpace(band.tim))
            {
                var SearchQuery = band.tim.Trim();
                collection = collection.Where(c => c.Name.Contains(SearchQuery));
            }
            #region page cũ
            //
            // // return collection
            // //     .Skip(band.PageSize * (band.PageNumber -1))
            // //     .Take(band.PageSize)
            // //     .ToList();
            // // chuyển các tham số tài nguyên vào băng tần thuộc tính
            //
            #endregion

            if (!string.IsNullOrWhiteSpace(band.OrderBy))
            {
                var bandPropertyMappingDictionary = _propertyMappingService.GetPropertyMapping<BandDto, Band>();
                collection.ApplySort(band.OrderBy, bandPropertyMappingDictionary);
            }

            return PagedList<Band>.Create(collection, band.PageNumber, band.PageSize);
        }
        public async Task<Band> AddBand2(Band band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));
            _context.Bands.Add(band);
            await _context.SaveChangesAsync();
            return band;
        }

        public void AddBand(Band band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));
            _context.Bands.Add(band);
        }

        public void UpdateBand(Band band)
        {
            // throw new NotImplementedException();
        }

        public void DeleteBand(Band band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));

            _context.Bands.Remove(band);
        }
        #endregion

        #region CRUD BandConvert
        public async Task<ResponseData.Response> getBand(BandQueryModel filter)
        {
            try
            {
                var predicate = BuildQueryBand(filter);
                var result = _context.Bands.Where(predicate).GetPage(filter);
                return new ResponseData.ResponsePagination<Band>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        public async Task<ResponseData.Response> getIdBand(Guid Id)
        {
            try
            {
                var entity = await _context.Bands.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                    return new ResponseData.ResponseError(Code.NotFound, "không tìm thấy Id ");
                var data = _mapper.Map<Band, BandDto>(entity);
                return new ResponseData.ResponseObject<BandDto>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        public async Task<ResponseData.Response> deleteBand(Guid Id)
        {
            try
            {
                var entity = await _context.Bands.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseData.ResponseError(Code.NotFound, "không tìm thấy id muốn xóa");
                }

                _context.Remove(entity);
                _context.SaveChanges();
                var name = entity.Name;
                return new ResponseData.ResponseDelete(Code.OK, "xóa thành công",Id,name);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }
        public async Task<ResponseData.Response> createBand(BandCreateModel model)
        {
            try
            {
                var entityModel = new Band()
                {
                    Founded = model.Founded,
                    Name = model.Name,
                    MainGenre = model.MainGenre
                };
                _context.Add(entityModel);

                var status = await _context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _mapper.Map<Band, BandDto>(entityModel);

                    return new ResponseData.ResponseObject<BandDto>(data, "Thêm thành công");

                }

                return new ResponseData.ResponseError(Code.NotFound, "Thêm thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<ResponseData.Response> updateBand(Guid Id, BandUpdateModel model)
        {
            try
            {
                var entityModel = await _context.Bands.Where(c => c.Id == Id).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseData.ResponseError(Code.NotFound, "Không tìm thấy Id trong Band");
                }
                entityModel.Founded = new DateTime();
                entityModel.Name = model.Name;
                entityModel.MainGenre = model.MainGenre;
                var status = await _context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _mapper.Map<Band, BandDto>(entityModel);

                    return new ResponseData.ResponseObject<BandDto>(data, "Sửa thành công");

                }
                return new ResponseData.ResponseError(Code.NotFound, "Thêm thất bại");
            }
            catch (Exception ex)
            {

                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }



        #endregion

        #region CRUD AlbumConvert

        public async Task<ResponseData.Response> updatelbum(Guid albumId, AlbumUpdateModel model)
        {
            try
            {
                var entityModel = await _context.Albums.Where(c => c.Id == albumId).FirstOrDefaultAsync();
                if (entityModel == null)
                {
                    return new ResponseData.ResponseError(Code.NotFound, "Không tìm thấy Id trong Band");
                }

                entityModel.Description = model.Description;
                entityModel.Tille = model.Tille;
                var status = await _context.SaveChangesAsync();
                if (status > 0)
                {
                    var data = _mapper.Map<Album, AlbumModel>(entityModel);
                    return new ResponseData.ResponseObject<AlbumModel>(data, "Sửa thành công");
                }
                return new ResponseData.ResponseError(Code.NotFound, "Sửa thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);

            }
        }
        public async Task<ResponseData.Response> deleteAlbum(Guid albumId)
        {
            try
            {
                var entity = await _context.Albums.Where(c => c.Id == albumId).FirstOrDefaultAsync();
                if (entity == null)
                {
                    return new ResponseData.ResponseError(Code.NotFound, "không tìm thấy id muốn xóa");
                }
                _context.Remove(entity);
                _context.SaveChanges();
                var tile = entity.Tille;
                return new ResponseData.ResponseDelete(Code.OK, "Xóa thành công",albumId,tile);

            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }

         public async Task<ResponseData.Response> getAlbums(AlbumQueryModel filter)
         {
            try
            {
                var predicate = BuildQueryAlbum(filter);
                var result = _context.Albums.Include(c=>c.bandAlbum).Where(predicate).GetPage(filter);
                return new ResponseData.ResponsePagination<Album>(result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
         }
        public async Task<ResponseData.Response> createAlbum(AlbumCreateModel model)
        {
            try
            {
                var entityModel = new Album()
                {
                   
                    Description = model.Description,
                    Tille = model.Tille,
                    BanID = model.Band,
                };
                _context.Add(entityModel);

                var status = await _context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _mapper.Map<Album, AlbumDto>(entityModel);

                    return new ResponseData.ResponseObject<AlbumDto>(data, "Thêm thành công");

                }

                return new ResponseData.ResponseError(Code.NotFound, "Thêm thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<ResponseData.Response> getIdAlbum(Guid albumId)
        {
            try
            {
                var entity = await _context.Albums.Include(c => c.bandAlbum).Where(c => c.Id == albumId)
                    .FirstOrDefaultAsync();
                if (entity == null)
                    return new ResponseData.ResponseError(Code.NotFound, "Không tìm thấy id Album");
                var data = _mapper.Map<Album,AlbumModel>(entity);
                return new ResponseData.ResponseObject<AlbumModel>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseData.ResponseError(Code.InternalServerError, ex.Message);
            }
        }


        #endregion

        private Expression<Func<Album, bool>> BuildQueryAlbum(AlbumQueryModel query)
        {
            var predicate = PredicateBuilder.New<Album>(true);

            if (query.AlbumId.HasValue && query.AlbumId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.AlbumId);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.Tille.Contains(query.FullTextSearch)
                                   || c.Description.Contains(query.FullTextSearch));

            return predicate;
        }
        private Expression<Func<Band, bool>> BuildQueryBand(BandQueryModel query)
        {
            var predicate = PredicateBuilder.New<Band>(true);

            if (query.bandId.HasValue && query.bandId.Value != Guid.Empty)
            {
                predicate = predicate.And(c => c.Id == query.bandId);
            }

            if (!string.IsNullOrEmpty(query.FullTextSearch))
                predicate.And(c => c.Name.Contains(query.FullTextSearch)
                                   || c.MainGenre.Contains(query.FullTextSearch));

            return predicate;
        }


        public bool BandExit(Guid bandId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            return _context.Albums.Any(c => c.Id == bandId);
        }

        public bool AlbumExit(Guid albumId)
        {
            if (albumId == Guid.Empty)
                throw new ArgumentNullException(nameof(albumId));
            //Xác định xem phần tử nào của chuỗi thỏa mãn điều kiện
            return _context.Albums.Any(c => c.Id == albumId);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}
