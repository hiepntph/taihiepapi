﻿using System;
using System.Collections.Generic;
using System.Text;
using Inmergers.Common.Utils;


namespace WebApplication1.Servises.bandandAlbum
{
    public class BandModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
    }
    public class BandCreateModel
    {

        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
    }
    public class BandUpdateModel
    {

        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
    }

    public class BandQueryModel : PaginationRequest
    {
        public Guid? bandId { get; set; }
    }


}
