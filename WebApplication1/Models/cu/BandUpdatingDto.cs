﻿namespace WebApplication1.Models.NewFolder
{
    public class BandUpdatingDto
    {
        public string Name { get; set; }
        public string Founded { get; set; }
        public string MainGenre { get; set; }
    }
}
