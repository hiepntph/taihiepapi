﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace WebApplication1.Models.NewFolder
{
    public class BandForCreatingDto
    {
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGenre { get; set; }
        // tạo mới và ánh xạ sang bên bảng
        public ICollection<AlbumForCreatingDto> Albums { get; set; } = new List<AlbumForCreatingDto>();
    }
}
