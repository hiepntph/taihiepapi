﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models.NewFolder
{
    public class BandDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FounndedYearAgo { get; set; }
        public string MainGenre { get; set; }
    }
}
