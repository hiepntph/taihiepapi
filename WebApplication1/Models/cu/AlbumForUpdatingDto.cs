﻿using System.ComponentModel.DataAnnotations;
using WebApplication1.ValtidationAttributesd;

namespace WebApplication1.Models.NewFolder
{

    public class AlbumForUpdatingDto
    {
        [Required]
        [MaxLength(200, ErrorMessage = "không được nhập quá 200 kí tự tiêu đề")]
        public string Tille { get; set; }

        [MaxLength(400, ErrorMessage = "không được nhập quá 400 kí tự mô tả")]
        public string Description { get; set; }
    }
}
