﻿using System.ComponentModel.DataAnnotations;
using WebApplication1.ValtidationAttributesd;

namespace WebApplication1.Models.NewFolder
{
    [ValidationAlbums(ErrorMessage = "chọc vào thằng này đầu")]
    public abstract class AlbumManipulationDto
    {
        [Required]
        [MaxLength(200, ErrorMessage = "không được nhập quá 200 kí tự tiêu đề")]
        public string Tille { get; set; }

        [MaxLength(400, ErrorMessage = "không được nhập quá 400 kí tự mô tả")]
        public virtual string Description { get; set; }
    }
}
