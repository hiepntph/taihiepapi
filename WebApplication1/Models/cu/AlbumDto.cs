﻿using System;
using WebApplication1.Entities;

namespace WebApplication1.Models.NewFolder
{
    public class AlbumDto
    {
        public Guid Id { get; set; }
        public string Tille { get; set; }
        public string Description { get; set; }
        public Guid BanID { get; set; }
    }
}
