﻿using WebApplication1.Entities;
using WebApplication1.Models.NewFolder;
using WebApplication1.Servises.bandandAlbum;

namespace WebApplication1.Profile
{
    public class AlbumsProfile : AutoMapper.Profile
    {
        public AlbumsProfile()
        {
            CreateMap<Entities.Album, AlbumDto>().ReverseMap();
            CreateMap<Entities.Album, AlbumModel>().ReverseMap();
            // //cho phép di chuyển thuộc tính của 2 bảng
            // CreateMap<AlbumForCreatingDto, Entities.Album>();
            // CreateMap<AlbumForUpdatingDto, Entities.Album>().ReverseMap();
        }
    }
}
