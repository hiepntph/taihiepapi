﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Helpers;
using WebApplication1.Models.NewFolder;

namespace WebApplication1.Profile

{
    public class BandsProfile : AutoMapper.Profile
    {
        public BandsProfile()
        {
            CreateMap<Entities.Band, BandDto>()
                .ForMember(c =>c.FounndedYearAgo,
                a =>a.MapFrom(l => $"{l.Founded.ToString("yyyy")}({l.Founded.GetYearsAgo() }năm)"))
                ;
            CreateMap<BandForCreatingDto, Entities.Band>();
            CreateMap<BandUpdatingDto, Entities.Band>().ReverseMap();

        }
    }
}
