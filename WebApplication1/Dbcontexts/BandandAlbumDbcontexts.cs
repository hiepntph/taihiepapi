﻿using System;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Entities;

namespace WebApplication1.Dbcontexts

{
    public class BandandAlbumDbcontexts : DbContext
    {

        public BandandAlbumDbcontexts(DbContextOptions<BandandAlbumDbcontexts> options) : base(options)
        {

        }
        public DbSet<Band> Bands { get; set; }
        public DbSet<Album> Albums { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Band>().HasData(new Band()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120002"),
                Name = "hiep",
                Founded = new DateTime(2002, 12, 6),
                MainGenre = "hanhdong"
            },
            new Band()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120000"),
                Name = "hai",
                Founded = new DateTime(2002, 12, 6),
                MainGenre = "tamly"
            },
            new Band()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120001"),
                Name = "babch",
                Founded = new DateTime(2002, 12, 6),
                MainGenre = "tinhcam"
            });

            modelBuilder.Entity<Album>().HasData(new Album()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120033"),
                Tille = "hanhdong",
                Description = "Mô tả 1",
                BanID = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120002"),
            },
            new Album()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120088"),
                Tille = "tamly",
                Description = "Mô tả 2",
                BanID = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120000"),
            },
            new Album()
            {
                Id = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120099"),
                Tille = "tinhcam",
                Description = "Mô tả 3",
                BanID = Guid.Parse("f62c1170-7f4c-11ed-a1eb-0242ac120001"),
            }
            );
            base.OnModelCreating(modelBuilder);
        }
    }
}
