﻿using System.ComponentModel.DataAnnotations;
using WebApplication1.Models.NewFolder;

namespace WebApplication1.ValtidationAttributesd
{
    //check cuối cùng
    public class ValidationAlbums : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var album = (AlbumManipulationDto)validationContext.ObjectInstance;

            if (album.Tille == album.Description)
            {
                 return new ValidationResult("không được để tiêu đề và mô tả giống nhau",
                    new[] { "AlbumManipulationDto" });
            }

            return ValidationResult.Success;
        }

    }
}
