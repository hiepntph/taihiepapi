﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace WebApplication1.Helpers
{
    public static class IEnumerableExtension
    {
        public static IEnumerable<ExpandoObject> ShapeDaTa<TSource>(this IEnumerable<TSource> sources, string fields)
        {
            if (sources == null)
                throw new ArgumentNullException(nameof(sources));
            var objectList = new List<ExpandoObject>();
            var propertyInfoList = new List<PropertyInfo>();
            if (string.IsNullOrWhiteSpace(fields))
            {
                var propertyInfos = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                propertyInfoList.AddRange(propertyInfos);
            }
            else
            {
                var fieldsAfterSplit = fields.Split(",");
                foreach (var field in fieldsAfterSplit)
                {
                    var propertyName = field.Trim();
                    var propertyInfo = typeof(TSource).GetProperty(propertyName,BindingFlags.Public | BindingFlags.Instance);
                    if (propertyInfo == null)
                        throw new Exception(propertyName.ToString() + " was not found");
                    propertyInfoList.Add(propertyInfo);
                }
            }

            foreach (TSource sourceObject in sources)
            {
                var dataShapedObject = new ExpandoObject();
                foreach (var propertyInfo in propertyInfoList)
                {
                    var propertyValue = propertyInfo.GetValue(sourceObject);
                    ((IDictionary<string,object>) dataShapedObject).Add(propertyInfo.Name,propertyValue);
                }
                objectList.Add(dataShapedObject);
            }

            return objectList;
        }
    }
}
