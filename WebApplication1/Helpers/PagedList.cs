﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace WebApplication1.Helpers
{
    //Generics
    //
    public class PagedList<T> : List<T>
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool HasPrevious => (CurrentPage > 1);// trả về true
        public bool HasNext => (CurrentPage < TotalPages);

        //đối số giá trị truyền vào khi gọi
        public PagedList(List<T> items, int totalCount, int currentPage, int pageSize)
        {
            // gán giá trị cho các thuộc tính của cột
            TotalCount = totalCount;
            CurrentPage = currentPage;
            PageSize = pageSize;
            //làm tròn số và chia user theo pageSize
            TotalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
            //thêm toàn bộ vào các phần tử vào items = list
            AddRange(items);
            //listT có thể truy ván 
        }

        // tạo lớp tính vào tái sử dụng pagedList<t> 
        //sử dụng lớp tĩnh để truy vấn dữ liệu
        public static PagedList<T> Create(IQueryable<T> source, int pageNumber, int pageSize)
        {
            var count = source.Count();// truy vấn đếm số phần tử trong count
            //bỏ qua pageNumber -1 * pageSize lấy lại giá trị page size và trả về 1 list
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PagedList<T>(items, count, pageNumber, pageSize);
        }
    }
}
