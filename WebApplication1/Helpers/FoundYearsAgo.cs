﻿using System;
using System.Runtime.InteropServices.ComTypes;

namespace WebApplication1.Helpers
{
    public static class FoundYearsAgo
    {
        public static int GetYearsAgo(this DateTime dateTime)
        {
            var year = DateTime.Now;
            int yearnow = year.Year - dateTime.Year;
            return yearnow;
        }
    }
}
