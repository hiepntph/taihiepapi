﻿using System.Runtime.InteropServices;

namespace WebApplication1.Helpers
{
    public class BandsResourceParameters
    {
        public string loc { get; set; }
        public string tim { get; set; }


        const int maxPageSize = 13;
        public int PageNumber { get; set; } = 1;

        private int _pageSize = 3;
        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > maxPageSize) ? maxPageSize : value;
        }

        public string OrderBy { get; set; } = "Name";
        public string Fields { get; set; }
    }
}
