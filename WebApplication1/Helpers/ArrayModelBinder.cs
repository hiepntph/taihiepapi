﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper.Execution;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WebApplication1.Helpers
{
    public class ArrayModelBinder : IModelBinder // cung cấp một cách để định nghĩa cách mà ASP.NET Core sẽ chuyển đổi dữ liệu từ request HTTP thành các đối tượng model trong ứng dụng của bạn.
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (!bindingContext.ModelMetadata.IsEnumerableType)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return  Task.CompletedTask;
            }

            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).ToString();

            if (string.IsNullOrWhiteSpace(value))
            {
                bindingContext.Result = ModelBindingResult.Success(null);
                return Task.CompletedTask;
            }

            var elementType = bindingContext.ModelType.GetTypeInfo().GenericTypeArguments[0];
            var converter = TypeDescriptor.GetConverter(elementType);
            var values = value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(c => converter.ConvertFromString(c.Trim())).ToArray();
            var typeValues = Array.CreateInstance(elementType, values.Length);
            
            values.CopyTo(typeValues, 0);
            bindingContext.Model = typeValues;
            
            bindingContext.Result = ModelBindingResult.Success(bindingContext.Model);
            return Task.CompletedTask;
        }
    }
}
