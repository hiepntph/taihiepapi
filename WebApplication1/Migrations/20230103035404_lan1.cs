﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Migrations
{
    public partial class lan1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bands",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Founded = table.Column<DateTime>(nullable: false),
                    MainGenre = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bands", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Albums",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Tille = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    BanID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Albums", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Albums_Bands_BanID",
                        column: x => x.BanID,
                        principalTable: "Bands",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120002"), new DateTime(2002, 12, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "hanhdong", "hiep" });

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120000"), new DateTime(2002, 12, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "tamly", "hai" });

            migrationBuilder.InsertData(
                table: "Bands",
                columns: new[] { "Id", "Founded", "MainGenre", "Name" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120001"), new DateTime(2002, 12, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "tinhcam", "babch" });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "BanID", "Description", "Tille" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120033"), new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120002"), "Mô tả 1", "hanhdong" });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "BanID", "Description", "Tille" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120088"), new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120000"), "Mô tả 2", "tamly" });

            migrationBuilder.InsertData(
                table: "Albums",
                columns: new[] { "Id", "BanID", "Description", "Tille" },
                values: new object[] { new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120099"), new Guid("f62c1170-7f4c-11ed-a1eb-0242ac120001"), "Mô tả 3", "tinhcam" });

            migrationBuilder.CreateIndex(
                name: "IX_Albums_BanID",
                table: "Albums",
                column: "BanID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Albums");

            migrationBuilder.DropTable(
                name: "Bands");
        }
    }
}
