﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Entities
{
    public class Album
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Chưa nhập tiêu đề")]
        [MaxLength(200)]
        public string Tille { get; set; }
        [MaxLength(100)]
        public string Description { get; set; }
        [ForeignKey("BanID")]
        public virtual Band bandAlbum { get; set; }
        public Guid BanID { get; set; }
        

    }
}
