﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Entities
{
    public class Band
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        [Required]
        [MaxLength(50)]
        public string MainGenre { get; set; }
        //ICollection định nghĩa một tập hợp các phần tử và cung cấp các phương thức quản lý
         // public ICollection<Album> Albums { get; set; } = new List<Album>();
      
    }
}
