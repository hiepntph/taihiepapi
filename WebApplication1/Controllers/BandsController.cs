﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Common.Utils;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Entities;
using WebApplication1.Helpers;
using WebApplication1.Models.NewFolder;
using WebApplication1.Servises.bandandAlbum;
using WebApplication1.Servises.sp;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/bands")]
    public class BandsController : ControllerBase
    {
        private readonly IbandAndAlbumRepositorycs _bandAndAlbumRepositorycs;
        private readonly IMapper _mapper;
        private readonly IPropertyMappingService _propertyMappingService;
        private readonly IPropertyValidationService _propertyValidationService;
        public BandsController(
            IbandAndAlbumRepositorycs bandAndAlbumRepositorycs,
            IMapper mapper, 
            IPropertyMappingService propertyMappingService,
            IPropertyValidationService propertyValidationService)
        {
            _bandAndAlbumRepositorycs = bandAndAlbumRepositorycs ?? throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
            _propertyMappingService = propertyMappingService ?? throw new ArgumentNullException(nameof(propertyMappingService));
            _propertyValidationService = propertyValidationService ?? throw new ArgumentNullException(nameof(propertyValidationService));

        }

        #region CrudConvert
        [HttpGet]
        public async Task<IActionResult> GetBands([FromQuery] BandQueryModel band)
        {
            var bandRepo = await _bandAndAlbumRepositorycs.getBand(band);
            return Helper.TransformData(bandRepo);
        }


        [HttpGet("{bandId}")]
        public async Task<IActionResult> GetIdBand(Guid bandId)
        {
            var result = await _bandAndAlbumRepositorycs.getIdBand(bandId);

            return Helper.TransformData(result);
        }

        [HttpDelete("{bandId}")]
        public async Task<IActionResult> DeleteBand(Guid bandId)
        {
            var result = await _bandAndAlbumRepositorycs.deleteBand(bandId);
            return Helper.TransformData(result);
        }
        [HttpPost]
        public async Task<IActionResult> CreateBand([FromBody] BandCreateModel band)
        {
            var result = await _bandAndAlbumRepositorycs.createBand(band);

            return Helper.TransformData(result);
        }
        [HttpPut]
        public async Task<IActionResult> UpdateBand(Guid Id, [FromBody] BandUpdateModel band)
        {
            var result = await _bandAndAlbumRepositorycs.updateBand(Id, band);

            return Helper.TransformData(result);
        }

        [HttpOptions]
        public IActionResult GetBandsOptions()
        {
            Response.Headers.Add("Allow", "GET,POST,DELETE,HEAD,OPTIONS");
            return Ok();
        }

        #endregion


        #region CRUD bandcu
        // [HttpPut("{bandId}")]
        // public ActionResult UpdateBand(Guid bandId, [FromBody] BandUpdatingDto band)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //     {
        //         return NotFound();
        //     }
        //     var albumFromRepo = _bandAndAlbumRepositorycs.getBand(bandId);
        //     if (albumFromRepo == null)
        //         return NotFound();
        //
        //     _mapper.Map(band, albumFromRepo);
        //     _bandAndAlbumRepositorycs.UpdateBand(albumFromRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }

        // [HttpGet("/api/bands/{banId}")]
        // public IActionResult GetBand(Guid banId)
        // {
        //     var bandsFromRepo = _bandAndAlbumRepositorycs.getBand(banId);
        //     if (bandsFromRepo == null)
        //         return NotFound();
        //     return Ok(bandsFromRepo);
        // }
        // [HttpGet(Name = "GetBands")]
        // [HttpHead]
        // public IActionResult GetBands([FromQuery] BandsResourceParameters band)
        // {
        //     if (!_propertyMappingService.ValidMappingExit<BandDto, Band>(band.OrderBy))
        //         return BadRequest();
        //     if (!_propertyValidationService.HasValidProperties<BandDto>(band.Fields))
        //         return BadRequest();
        //
        //         var bandsFromRepo = _bandAndAlbumRepositorycs.GetBands(band);
        //     #region code cũ chọc thẳng vào không qua lớp nào
        //
        //     // var bandDto = new List<BandDto>();
        //     // foreach (var band in bandsFromRepo)
        //     // {
        //     //     bandDto.Add( 
        //     //         new BandDto()
        //     //         {
        //     //             ID = band.ID,
        //     //             Name = band.Name,
        //     //             MainGenre = band.MainGenre,
        //     //             // sử dụng phép nội suy cho chèn các giá trị biểu thức,có thể thay thế biến hoặc mở rộng
        //     //             FounndedYearAgo = $"{band.Founnded.ToString("yyyy")}({band.Founnded.GetYearsAgo()}năm)"
        //     //         }
        //     //     );
        //     //      
        //     // }
        //
        //     #endregion
        //
        //     var previosPageLink = bandsFromRepo.HasPrevious ? CreateBandsUri(band, UriType.PreviousPage) : null;
        //     var nextPageLink = bandsFromRepo.HasNext ? CreateBandsUri(band, UriType.PreviousPage) : null;
        //
        //     var metaData = new
        //     {
        //         totalCount = bandsFromRepo.TotalCount,
        //         pageSize = bandsFromRepo.PageSize,
        //         currentPage = bandsFromRepo.CurrentPage,
        //         totalPages = bandsFromRepo.TotalPages,
        //         previosPageLink = previosPageLink,
        //         nextPageLink = nextPageLink,
        //     };
        //     Response.Headers.Add("Pagination", JsonSerializer.Serialize(metaData));
        //     // tham chiếu đến banDto và tham số 
        //     return Ok(_mapper.Map<IEnumerable<BandDto>>(bandsFromRepo).ShapeDaTa(band.Fields));
        // }

        // [HttpPost]
        // public ActionResult<BandDto> CreateBand([FromBody] BandForCreatingDto band)
        // {
        //     var bandEntity = _mapper.Map<Band>(band);
        //     _bandAndAlbumRepositorycs.AddBand(bandEntity);
        //     _bandAndAlbumRepositorycs.Save();
        //
        //     //var bandToReturn = _mapper.Map<BandDto>(bandEntity);
        //     return Ok();
        // }

        // [HttpPut("{bandId}")]
        // public ActionResult UpdateBand(Guid bandId, [FromBody] BandUpdatingDto band)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //     {
        //         return NotFound();
        //     }
        //     var albumFromRepo = _bandAndAlbumRepositorycs.getBand(bandId);
        //     if (albumFromRepo == null)
        //         return NotFound();
        //
        //     _mapper.Map(band, albumFromRepo);
        //     _bandAndAlbumRepositorycs.UpdateBand(albumFromRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }
        // [HttpDelete("{bandId}")]
        // public ActionResult deleteBand(Guid bandId)
        // {
        //     var bandFromRepo = _bandAndAlbumRepositorycs.getBand(bandId);
        //     if (bandFromRepo == null)
        //         return NotFound();
        //     _bandAndAlbumRepositorycs.DeleteBand(bandFromRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }
        // private string CreateBandsUri(BandsResourceParameters bands, UriType uriType)
        // {
        //     switch (uriType)
        //     {
        //         case UriType.PreviousPage:
        //             return Url.Link("GetBands", new
        //             {
        //                 fiels = bands.Fields,
        //                 orderBy = bands.OrderBy,
        //                 pageNumber = bands.PageNumber - 1,
        //                 pageSize = bands.PageSize,
        //                 mainGenre = bands.loc,
        //                 searchQuery = bands.tim
        //             });
        //         case UriType.NextPage:
        //             return Url.Link("GetBands", new
        //             {
        //                 fiels = bands.Fields,
        //                 orderBy = bands.OrderBy,
        //                 pageNumber = bands.PageNumber + 1,
        //                 pageSize = bands.PageSize,
        //                 mainGenre = bands.loc,
        //                 searchQuery = bands.tim
        //             });
        //         default:
        //             return Url.Link("GetBands", new
        //             {
        //                 fiels = bands.Fields,
        //                 orderBy = bands.OrderBy,
        //                 pageNumber = bands.PageNumber + 1,
        //                 pageSize = bands.PageSize,
        //                 mainGenre = bands.loc,
        //                 searchQuery = bands.tim
        //             });
        //     }
        // }
        #endregion



    }
}
