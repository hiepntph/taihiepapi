﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Inmergers.Common.Utils;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApplication1.Entities;
using WebApplication1.Models.NewFolder;
using WebApplication1.Servises.bandandAlbum;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/albums")]
    public class AlbumController : Controller
    {

        private readonly IbandAndAlbumRepositorycs _bandAndAlbumRepositorycs;
        private readonly IMapper _mapper;

        public AlbumController(IbandAndAlbumRepositorycs bandAndAlbumRepositorycs, IMapper mapper)
        {
            _bandAndAlbumRepositorycs = bandAndAlbumRepositorycs ??
                                        throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
        }

        #region crudConvert

        [HttpGet]
        public async Task<IActionResult> GetAlbums([FromQuery] AlbumQueryModel album)
        {
            var AlbumRepo = await _bandAndAlbumRepositorycs.getAlbums(album);
            return Helper.TransformData(AlbumRepo);
        }

        [HttpGet("{albumId}")]
        public async Task<IActionResult> GetIdAlbums(Guid albumId)
        {
            var AlbumRepo = await _bandAndAlbumRepositorycs.getIdAlbum(albumId);
            return Helper.TransformData(AlbumRepo);
        }
        [HttpPost]
        public async Task<IActionResult> CreateAlbum([FromBody] AlbumCreateModel album)
        {
            var result = await _bandAndAlbumRepositorycs.createAlbum(album);

            return Helper.TransformData(result);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteAlbum(Guid albumId)
        {
            var result = await _bandAndAlbumRepositorycs.deleteAlbum(albumId);
            return Helper.TransformData(result);
        }


        [HttpPut]
        public async Task<IActionResult> UpdateAlbum(Guid albumId , [FromBody] AlbumUpdateModel album)
        {
            var result = await _bandAndAlbumRepositorycs.updatelbum(albumId,album);
            return Helper.TransformData(result);
        }

        #endregion

        #region crudcu
        //
        //
        //
        //
        // [HttpGet]
        // public ActionResult<IEnumerable<AlbumDto>> GetAlbumsForBand(Guid bandId)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //         return NotFound();
        //     var albumFromRepo = _bandAndAlbumRepositorycs.getAlbums(bandId);
        //     return Ok(_mapper.Map<IEnumerable<AlbumDto>>(albumFromRepo));
        // }
        //
        // [HttpGet("{albumId}", Name = "GetAlbum")]
        // public ActionResult<AlbumDto> GetAlbum(Guid bandId, Guid albumId)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //         return NotFound();
        //     var albumForRepo = _bandAndAlbumRepositorycs.getAlbum(bandId, albumId);
        //     if (albumForRepo == null)
        //         return NotFound();
        //     return Ok(_mapper.Map<AlbumDto>(albumForRepo));
        // }
        //
        // [HttpPost]
        // public ActionResult<AlbumDto> CreateAlbum(Guid bandId, [FromBody] AlbumForCreatingDto album)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //         return NotFound();
        //
        //     var albumEntity = _mapper.Map<Album>(album);
        //     _bandAndAlbumRepositorycs.addAlbum(bandId, albumEntity);
        //     _bandAndAlbumRepositorycs.Save();
        //     // tạo đầu ra
        //     var albumToReturn = _mapper.Map<AlbumDto>(albumEntity);
        //     // tạo reponse 201 và chứa url của thứ mới tạo
        //     return CreatedAtRoute("GetAlbum", new { bandId = bandId, albumId = albumToReturn.Id }, albumToReturn);
        // }
        //
        // [HttpPut("{albumId}")]
        // public ActionResult UpdateAlbumForBand(Guid bandId, Guid albumId, [FromBody] AlbumForUpdatingDto album)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //     {
        //         return NotFound();
        //     }
        //     var albumFromRepo = _bandAndAlbumRepositorycs.getAlbum(bandId, albumId);
        //     if (albumFromRepo == null)
        //     {
        //         var albumToAdd = _mapper.Map<Album>(album);
        //         albumToAdd.Id = albumId;// thằng updateDto không có id nên phải thêm vào
        //         _bandAndAlbumRepositorycs.addAlbum(bandId, albumToAdd);
        //         _bandAndAlbumRepositorycs.Save();
        //
        //         var albumToReTurn = _mapper.Map<AlbumDto>(albumToAdd);
        //         return CreatedAtAction("GetAlbum", new { bandId = bandId, albumId = albumToReTurn.Id }, albumToReTurn);
        //     }
        //
        //         _mapper.Map(album, albumFromRepo);
        //     _bandAndAlbumRepositorycs.uppdateAlbum(albumFromRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }
        //
        // //JsonPatchDocument thay đổi trên 1 đối tượng chứ không phải toàn bộ nó      
        // [HttpPatch("{albumId}")]
        // public ActionResult partiallyUpdateAlbumForBand(Guid bandId, Guid albumId, [FromBody] JsonPatchDocument<AlbumForUpdatingDto> patchDocument)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //         return NotFound();
        //     var albumFromRepo = _bandAndAlbumRepositorycs.getAlbum(bandId, albumId);
        //     if (albumFromRepo == null)
        //         return NotFound();
        //     // {
        //     //     var albumDto = new AlbumForUpdatingDto();
        //     //     patchDocument.ApplyTo(albumDto);
        //     //     var albumToAdd = _mapper.Map<Album>(albumDto);
        //     //     albumToAdd.Id  = albumId;
        //     //     _bandAndAlbumRepositorycs.addAlbum(bandId,albumToAdd);
        //     //     _bandAndAlbumRepositorycs.Save();
        //     //     var albumToReTurn = _mapper.Map<AlbumDto>(albumToAdd);
        //     //     return CreatedAtAction("GetAlbum", new { bandId = bandId, albumId = albumToReTurn.Id }, albumToReTurn);
        //     // }
        //
        //     var albumToPacth = _mapper.Map<AlbumForUpdatingDto>(albumFromRepo);
        //     patchDocument.ApplyTo(albumToPacth);
        //
        //     _mapper.Map(albumToPacth, albumFromRepo);
        //     _bandAndAlbumRepositorycs.uppdateAlbum(albumFromRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }
        //
        // [HttpDelete("{albumId}")]
        // public ActionResult deleteAlbumsForBand(Guid bandId, Guid albumId)
        // {
        //     if (_bandAndAlbumRepositorycs.BandExit(bandId))
        //         return NotFound();
        //
        //     var albumFroRepo = _bandAndAlbumRepositorycs.getAlbum(bandId, albumId);
        //
        //     if (albumFroRepo == null)
        //         return NotFound();
        //
        //     _bandAndAlbumRepositorycs.deleteAlbum(albumFroRepo);
        //     _bandAndAlbumRepositorycs.Save();
        //     return NoContent();
        // }
        #endregion
    }
}
