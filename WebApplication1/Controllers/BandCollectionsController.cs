﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Helpers;
using WebApplication1.Models.NewFolder;
using WebApplication1.Servises.bandandAlbum;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/op")]
    public class BandCollectionsController : ControllerBase
    {
        private readonly IbandAndAlbumRepositorycs _bandAndAlbumRepositorycs;
        private readonly IMapper _mapper;
        public BandCollectionsController(IbandAndAlbumRepositorycs bandAndAlbumRepositorycs, IMapper mapper)
        {
            _bandAndAlbumRepositorycs = bandAndAlbumRepositorycs ?? throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(bandAndAlbumRepositorycs));
        }

        [HttpGet("{ids}",Name = "GetBandCollection")]
        public IActionResult GetBandCollection([FromRoute][ModelBinder(BinderType = typeof(ArrayModelBinder))]IEnumerable<Guid> ids)
        {
            if (ids == null)
                return BadRequest();
            var bandEntities = _bandAndAlbumRepositorycs.getBands(ids);
            if (ids.Count() != bandEntities.Count())
                return NotFound();
            var bandsToReturn = _mapper.Map<IEnumerable<BandDto>>(bandEntities);
            return Ok(bandsToReturn);
        }

        [HttpPost]
        public ActionResult<IEnumerable<BandDto>> CreateBandCollections(
            [FromBody] IEnumerable<BandForCreatingDto> bancollection)
        {
            var banEntities = _mapper.Map<IEnumerable<Entities.Band>>(bancollection);
            foreach (var band in banEntities)
            {
                _bandAndAlbumRepositorycs.AddBand(band);
            }
            
            _bandAndAlbumRepositorycs.Save();
            var bandCollectionToReturn = _mapper.Map<IEnumerable<BandDto>>(banEntities);
            var IdsString = string.Join(",", bandCollectionToReturn.Select(c => c.Id));
            
            return CreatedAtRoute("GetBandCollection", new { ids = IdsString }, bandCollectionToReturn);
        }

    }
}
